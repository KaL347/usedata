# UseData

Application web

Utilisation de la donnée d’une source pour partager, normaliser et exploiter un fichier.

Contexte

Le traitement des données informatiques concerne chaque individu dans son quotidien. Professionnellement toute entreprise est confrontée à la gestion de données hétérogènes.
Le projet consiste à simplifier et dématérialiser le traitement des données pour une plus grande qualité de l’exploitation.
Actuellement les différentes données stockées sont disponibles sous divers formats, dans différents services et en plusieurs lieux, rendant leur consultation ardue.
L’objectif est d’élaborer des passerelles ergonomiques entre les données et les personnes qui souhaitent les utiliser.
Concrètement il s’agit de proposer un outil générique multi support permettant de générer un document de synthèse transmissible immédiatement à un récepteur.
L’utilisateur dispose d’un logiciel portatif (web, application mobile) sur mesure en faisant appel à des données personnalisées mises en place spécifiquement.
